# -*- coding: utf-8 -*-
"""
Boundaries file for Ray-Tracing algorithm. All values of length in mm 
(because function of PALS lens is in mm). Written in mpmath

Created on Wed May 19 20:56:04 2021

@author: Acer
"""

import numpy as np
import mpmath as m
import warnings
import Config

def PALS_lens_polynom(x,y,normala):
    """
    Zakrivena cast PALSovske cocky.
    
    Parameters
    ---------
    x,y : m.mpf numbers
        udavaji polohy bodu x,y, kde se vycisli funkce
        
    normala : bool
        False znamena, ze chci vratit bod plochy z(x,y)
        True znamena, ze chci vratit normalovy vektor na plochu v bode [x,y,z(x,y)]
    Returns
    -------
    numpy array full of m.mpf values
    vrati vycislene hodnoty polynomu v bodech x,y, nebo normalu na plochu 
    [df/dx,df/dy,-1] v bode [x,y,f(x,y)] jako np.array plnou m.mpf hodnot
    """
    a = m.mpf("0.003165739")
    b = m.mpf("0.01635294")
    c = m.mpf("0.22274610e-8")
    d = m.mpf("0.15369828e-13")
    e = m.mpf("0.17898972e-18")
    if not normala:
        r2 = m.mpf(x)**2+m.mpf(y)**2
        result = (a*r2/(1+m.sqrt(1-(1-b)*a**2*r2))- c*r2**2 -d*r2**3 - e*r2**4)
# =============================================================================
#         result = (0.003165739*(x**2+y**2)/(1+np.sqrt(1-(1-0.01635294)*
#              0.003165739**2*(x**2+y**2)))-0.22274610e-8*(x**2+y**2)**2-
#              0.15369828e-13*(x**2+y**2)**3-0.17898972e-18*(x**2+y**2)**4)
# =============================================================================
    elif normala:
        result = np.array([m.mpf(0),m.mpf(0),m.mpf(-1)])
        result[0] = (a**3*x*(1 - b)*(x**2 + y**2)/
                    (m.sqrt(-a**2*(1 - b)*(x**2 + y**2) + 1)*
                    (m.sqrt(-a**2*(1 - b)*(x**2 + y**2) + 1) + 1)**2) + 
                    2*a*x/(np.sqrt(-a**2*(1 - b)*
                    (x**2 + y**2) + 1) + 1) - 4*c*x*(x**2 + y**2) -
                    6*d*x*(x**2 + y**2)**2 - 8*e*x*(x**2 + y**2)**3)
        result[1] = (a**3*y*(1 - b)*(x**2 + y**2)/
                    (m.sqrt(-a**2*(1 - b)*(x**2 + y**2) + 1)*
                    (m.sqrt(-a**2*(1 - b)*(x**2 + y**2) + 1) + 1)**2) + 
                    2*a*y/(m.sqrt(-a**2*(1 - b)*(x**2 + y**2) + 1) + 1) -
                    4*c*y*(x**2 + y**2) - 6*d*y*(x**2 + y**2)**2 -
                    8*e*y*(x**2 + y**2)**3)
    return result


def primka(x,y, normala):
    """
    Rovina pro lom.
    
    Parameters
    ---------
    x,y : m.mpf numbers
        udavaji polohy bodu x,y, kde se vycisli funkce
        
    normala : bool
        False znamena, ze chci vratit bod plochy z(x,y)
        True znamena, ze chci vratit normalovy vektor na plochu v bode [x,y,z(x,y)]
    Returns
    -------
    numpy array full of m.mpf values
    vrati vycislene hodnoty polynomu v bodech x,y, nebo normalu na plochu 
    [df/dx,df/dy,-1] v bode [x,y,f(x,y)] jako np.array plnou m.mpf hodnot
    """
    if not normala:
        result = m.mpf(0)
    elif normala:
        result = np.array([m.mpf(0),m.mpf(0),m.mpf(-1)])
    return result


def circle_left(x,y,normala):
    """
    Left boundary of sphere with radius R = 100
    (needs to be changed in function).
    
    Parameters
    ---------
    x,y : m.mpf numbers
        udavaji polohy bodu x,y, kde se vycisli funkce
        
    normala : bool
        False znamena, ze chci vratit bod plochy z(x,y)
        True znamena, ze chci vratit normalovy vektor na plochu v bode [x,y,z(x,y)]
    Returns
    -------
    numpy array full of m.mpf values
    vrati vycislene hodnoty polynomu v bodech x,y, nebo normalu na plochu 
    [df/dx,df/dy,-1] v bode [x,y,f(x,y)] jako np.array plnou m.mpf hodnot
    """
    R = m.mpf(100)
    r2 = m.mpf(x)**2+m.mpf(y)**2
    if r2 > R**2:
        print("chyba")
        warnings.warn("Prilis velky polomer")
    if not normala:
        result = R-m.sqrt(R**2-r2)
    elif normala:
        result = np.array([m.mpf(0),m.mpf(0),m.mpf(-1)])
        result[0] = x/m.sqrt(R**2-r2)
        result[1] = y/m.sqrt(R**2-r2)
    return result

def circle_right(x,y,normala):
    """
    Right boundary of sphere with radius R = 100
    (needs to be changed in function).
    
    Parameters
    ---------
    x,y : m.mpf numbers
        udavaji polohy bodu x,y, kde se vycisli funkce
        
    normala : bool
        False znamena, ze chci vratit bod plochy z(x,y)
        True znamena, ze chci vratit normalovy vektor na plochu v bode [x,y,z(x,y)]
    Returns
    -------
    numpy array full of m.mpf values
    vrati vycislene hodnoty polynomu v bodech x,y, nebo normalu na plochu 
    [df/dx,df/dy,-1] v bode [x,y,f(x,y)] jako np.array plnou m.mpf hodnot
    """
    R = m.mpf(100)
    r2 = m.mpf(x)**2+m.mpf(y)**2
    if r2 > R**2:
        print("chyba")
        warnings.warn("Prilis velky polomer")
    if not normala:
        result = R+m.sqrt(R**2-r2)
    elif normala:
        result = np.array([m.mpf(0),m.mpf(0),m.mpf(-1)])
        result[0] = -x/m.sqrt(R**2-r2)
        result[1] = -y/m.sqrt(R**2-r2)
    return result

    







