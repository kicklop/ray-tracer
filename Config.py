# -*- coding: utf-8 -*-
"""
Configuration file for Ray-Tracing algorithm. All values of length in mm 
(because function of PALS lens is in mm). Written in mpmath

Created on Wed May 19 20:56:04 2021

@author: Acer
"""


import mpmath as m

#setting the number of decimal places for mpmath
dps_normal = 20
dps_better = dps_normal + int(dps_normal/5)
dps_best = dps_normal + int(dps_normal/3)

m.mp.dps = dps_normal


tol_find_t = m.mpf(1e-15)
tol_refract = m.mpf(1e-15)






