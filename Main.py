# -*- coding: utf-8 -*-
"""
Main file for Ray-Tracing algorithm. All values of length in mm 
(because function of PALS lens is in mm). Written in mpmath

Created on Wed May 19 20:56:04 2021

@author: Acer
"""

import os
try:
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
except NameError:
    dname = "D:/OneDrive - České vysoké učení technické v Praze/diplomka/Ray-Tracer_Python/Ray-Tracer_divided"
os.chdir(dname)


import Functions as F
import Boundaries as B
import Classes as C
import Config

import numpy as np
import mpmath as m
import matplotlib.pyplot as plt
import warnings
import scipy.optimize
import time

#for debug
# =============================================================================
# try:
#     warnings.warn("some error")
# except:
#     extype, value, tb = sys.exc_info()
#     traceback.print_exc()
#     pdb.post_mortem(tb)
# =============================================================================
import pdb, traceback, sys

warnings.filterwarnings("error")
#%%
n_lambda = m.mpf("1.5035")
angle = [0,m.pi/2,0]
pt_of_rot = [0,0,"10"]
OG = [0, 0, 5]

rot_Rays = C.Rays(1,0,"line_x",[0,0,1],0)

rot_Rays.transform(OG, angle, pt_of_rot, False)
print(f"pozice je: {rot_Rays.position}")
print(f"smer je: {rot_Rays.direction}")
#%%
#438 nm => index lomu BK7 1.5265
#1315 nm => index lomu BK7 1.5035
#1310 nm, aby se f rovnalo f v ZEMAXU (650.194) => n_lambda = 1.503582913879877
#437 nm, aby se f rovnalo f v ZEMAXU (623.316) => n_lambda = 1.5265971890143568
n_lambda = m.mpf("1.5265971890143568")
cocka_elem = C.Opt_element([0,0,0],[0,0,0],[0,0,0],60,n_lambda, B.PALS_lens_polynom,B.primka, None, "17.5")
schutzplate_elem = C.Opt_element([0,0,70],[0,0,0],[0,0,0],8,n_lambda, B.primka,B.primka)
#filtr_elem = C.Opt_element([0,0,300],[0,0,0],[0,0,0],"7.3",n_lambda, B.primka,B.primka) #1315 nm
filtr_elem = C.Opt_element([0,0,300],[0,0,0],[0,0,0],"15.75",n_lambda, B.primka,B.primka) #438 nm

test_Rays = C.Rays(1000,145,"sunflower",[0,0,1],0)

test_Rays.propagate_through_opt_elem(cocka_elem)
test_Rays.propagate_through_opt_elem(schutzplate_elem)
test_Rays.propagate_through_opt_elem(filtr_elem)

z0 = test_Rays.find_smallest_radius('RMS')
test_Rays.plot_profile_in_pos_z(z0)
rRMS = test_Rays.radius(z0,"RMS")
rGEO = test_Rays.radius(z0,"GEO")
print(f"RMS radius is {rRMS}\nGEO radius is {rGEO}")
print(f"z0 = {z0}")

#%%
#cocka pod uhlem
#popis v deniku 5.4.2021, vzdalenost na kamere 204 um
#fokus pro 1315 nm je: 650295.21825089570566-60000
#627 mm je ohniskova vzdalenost cocky
#fokus pro 437 nm je (podle meho programu): 623.42463821565637208849017



n_lambda = m.mpf("1.5265")
angle = [0,"0.001",0]
pt_of_rot = [0,0,'47.2']
shift_xy = [0, 0]
cocka_elem_angled = C.Opt_element([*shift_xy,0],angle,pt_of_rot,60,n_lambda, B.PALS_lens_polynom,B.primka, None, "17.5")
schutzplate_elem_angled = C.Opt_element([*shift_xy,70],angle,pt_of_rot,8,n_lambda, B.primka,B.primka)

rot_Rays = C.Rays(200,150,"sunflower",[0,0,1],0)

rot_Rays.propagate_through_opt_elem(cocka_elem_angled)
rot_Rays.propagate_through_opt_elem(schutzplate_elem_angled)

rot_Rays.plot_profile_in_pos_z(m.mpf("623.42463821565637208849017"))

#%%
test_Rays = C.Rays(1,80,"line_x",[0,0,1],0)
print(f"smer paprsku je: {test_Rays.direction}")
test_Rays.propagate_to_boundary_and_refract(0, B.circle_left, 1., "1.5", 100)
print(f"poloha je: {test_Rays.position}")
print(test_Rays.direction)
print(f"toto cislo: {test_Rays.direction[0][0]/test_Rays.direction[0][2]}"+
      " by mělo byt rovne tomuto: 0.382")

print(test_Rays.find_smallest_radius("RMS"))
print("vysledek by mel byl 250")

#%%
kulata_cocka = C.Opt_element([40,0,0], [0,0,0], 0, 1.5, B.circle_left,B.circle_right,100)
test_Rays = C.Rays(1,-80,"line_x",[0,0,1],0)
test_Rays.propagate_through_opt_elem(kulata_cocka)
print(test_Rays.find_smallest_radius("RMS"))






