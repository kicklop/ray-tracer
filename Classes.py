# -*- coding: utf-8 -*-
"""
Classes file for Ray-Tracing algorithm. All values of length in mm 
(because function of PALS lens is in mm). Written in mpmath

Created on Wed May 19 20:56:04 2021

@author: Acer
"""

import Functions as F
import Config

import numpy as np
import mpmath as m
import matplotlib.pyplot as plt
import warnings
import scipy.optimize

class Rays:
    def __init__(self, num_pts, radius, method, direction, z_start_pos,
                 refr_index = m.mpf(1)):
        """
        Inicializator mnoha paprsku.
        
        Parameters
        ---------
        num_pts: int,
            pocet sledovanych paprsku
        radius : m.mpf number or string of number or int
            udava polomer kruhu sledovanych paprsku
        direction : array o rozmeru 3 prvku m.mpf or string or int
            udava smer paprsku jako vektor (v_x,v_y,v_z)
        z_start_pos : m.mpf or string or int
            pocatecni pozice z paprsku
        refr_index : m.mpf or string or int
            index lomu prostredi, kde paprsky zacinaji
        """
        OG_dps = F.upgrade_dps(Config.dps_better)
        x, y = F.gen_equid_points_on_disk(num_pts, m.mpf(radius), method)
        position_array = np.ndarray(shape=(num_pts,4),dtype = np.object)
        position_array[:,3] = m.mpf(1)
        position_array[:,2] = m.mpf(z_start_pos)
        position_array[:,1] = y
        position_array[:,0] = x
        F.restore_dps(OG_dps)
        
        direction_array = np.ndarray(shape=(num_pts,4), dtype = np.object)
        direction_array[:,3] = m.mpf(0)
        for i in range(3):
            direction_array[:,i] = m.mpf(direction[i])
        
        self.position = position_array
        #prvni osa je cislo paprsku, druha osa je [x,y,z,1], 1 kvuli transformaci
        self.direction = direction_array
        self.normalize_direction() #normalizuji
        self.refr_index = m.mpf(refr_index)
        #prvni osa je cislo paprsku, druha osa je [v_x,v_y,v_z,0], 0 kvuli transformaci
        #protoze vektor se nema transformovat treba pri translaci

    def normalize_direction(self):
        """
        Normalize direction vectors

        Returns
        -------
        None.

        """
        normed_magnitude = 1
        tmp_direction = np.empty_like(self.direction)
        OG_dps = F.upgrade_dps(Config.dps_better)
        for i, vector in enumerate(self.direction):
            norm = np.linalg.norm(vector)
            if norm != m.mpf(1):
                tmp_direction[i] = (normed_magnitude*vector/norm)
            else:
                tmp_direction[i] = vector
        F.restore_dps(OG_dps)
        self.direction = tmp_direction


    def propagate_to_z(self,z):
        """
        Siri paprsky volnym prostorem do polohy z.
        
        Parameters
        ---------
        z: m.mpf or string or int
            poloha, kam chci sirit
        """
        z = m.mpf(z)
        new_position = np.empty_like(self.position)
        OG_dps = F.upgrade_dps(Config.dps_better)
        for i, (point, vector) in enumerate(zip(self.position, self.direction)):
            z_diff = z - point[2]
            t = z_diff/vector[2]
            new_position[i] = point + vector*t
        F.restore_dps(OG_dps)
        self.position = new_position

    def plot_profile_in_pos_z(self, z):
        """
        Create 2D profile of rays in position z.
        !!! x-axis needs to be plotted inversely (with minus sign), so the 
        coordinate system is right-handed (careful when doing different plots)
        
        Parameters
        ---------
        z: m.mpf or string or int
            poloha, kam chci sirit
        """
        z = m.mpf(z)
        new_position = np.empty_like(self.position)
        OG_dps = F.upgrade_dps(Config.dps_better)
        for i, (point, vector) in enumerate(zip(self.position, self.direction)):
            z_diff = z - point[2]
            t = z_diff/vector[2]
            new_position[i] = point + vector*t
        F.restore_dps(OG_dps)
        fig, ax1 = plt.subplots()
        ax1.plot(-new_position[:,0],new_position[:,1], linewidth = 0,
                 marker = '.', markersize = 2)
        fig.set_size_inches(5,5)
        plt.show()
        plt.close(fig=fig)
        print("Right handed coordinate system.")
        
    def plot_rays(self,z_min,z_max,num_of_rays=None):
        """
        Makes a plot of rays. X-axis of plot is z-axis in rays,
        y-axis of plot is radius from (0,0,z)

        Parameters
        ----------
        z_min : m.mpf or string or int
            minimal value of z plotted
        z_max : m.mpf or string or int
            maximum value of z plotted
        num_of_rays : int
            number of rays to be plotted.
            If None or zero, then all rays are plotted.

        Returns
        -------
        None.
        """
        if not num_of_rays:
            step = 1
        else:
            step = int(self.position.shape[0]/num_of_rays)
        z_min = m.mpf(z_min)
        z_max = m.mpf(z_max)
        fig, ax1 = plt.subplots()
        for point, vector in zip(self.position[::step], self.direction[::step]):
            t_min = (z_min - point[2])/vector[2]
            t_max = (z_max - point[2])/vector[2]
            t_arr = m.linspace(t_min,t_max,100)
            t_arr = np.array(t_arr)
            z_arr = np.empty_like(t_arr)
            r_arr = np.empty_like(t_arr)
            for i,t in enumerate(t_arr):
                tmp_pos = (point + vector*t)
                z_arr[i] = tmp_pos[2]
                r_arr[i] = np.linalg.norm(tmp_pos[0:2])
            ax1.plot(z_arr,r_arr, linewidth = 1,
                     color='blue')
        fig.set_size_inches(5,5)
        plt.show()
        plt.close(fig=fig)
            
    def transform(self, new_origin, rotation_angles, point_of_rot,
                  to_starting_pos):
        """
        Transformuje paprsky do jine souradne soustavy. Vetsinou chci pro
        pruchod optickymi elementy.
        
        Parameters
        ----------
        new_origin : [x,y,z] in mpf, str, int
            poloha noveho pocatku souradnic (v souradne soustave paprsku)
        rotation_angles : [fi_x, fi_y, fi_z] in mpf, str
            uhly v radianech, o ktere je nova soustava
            rotovana. x,y,z odpovida ose rotace (kolem ktere osy rotuji).
            Znamenko uhlu podle pravidla prave ruky (palec ve smeru osy,
            kolem ktere rotuji), tj. prsty od puvodni ose k nove => kladny uhel.
        point_of_rot : [x,y,z] in mpf, str
            poloha bodu, kolem ktereho se rotuje (v souradne soustave paprsku)
        to_starting_pos : bool
            True, pokud se soustava vraci do puvodni pozice, kde paprsky zacaly.
            False, pokud se prechazi do soustavy rozhrani.
        Returns
        -------
        None.
        """
        #odvozeno v deniku 18.4.2021, 23.5.2021
        OG_dps = F.upgrade_dps(Config.dps_best)
        try:
            new_origin = new_origin.tolist()
        except AttributeError:
            pass
        new_origin.append(1) #so point can be translated
        new_origin = m.matrix(new_origin)
        try:
            point_of_rot = point_of_rot.tolist()
        except AttributeError:
            pass
        point_of_rot.append(0)
        point_of_rot = m.matrix(point_of_rot)
        rotation_angles = m.matrix(rotation_angles)
        
        trans_vector_to_rot_pt = point_of_rot*(-1)
        trans_matrix_to_rot_pt = m.eye(4)
        trans_matrix_to_rot_pt[:,3] = trans_matrix_to_rot_pt[:,3] + trans_vector_to_rot_pt
               
        fi_x,fi_y,fi_z = rotation_angles
        if fi_x:
            rot_x_matrix = m.matrix([[1,0,0,0],
                                     [0,m.cos(fi_x),m.sin(fi_x),0],
                                     [0,-m.sin(fi_x),m.cos(fi_x),0],
                                     [0,0,0,1]])
        else:
            rot_x_matrix = m.eye(4)
        if fi_y:
            rot_y_matrix = m.matrix([[m.cos(fi_y),0,-m.sin(fi_y),0],
                                     [0,1,0,0],
                                     [m.sin(fi_y),0,m.cos(fi_y),0],
                                     [0,0,0,1]])
        else:
            rot_y_matrix = m.eye(4)
        if fi_z:
            rot_z_matrix = m.matrix([[m.cos(fi_z),m.sin(fi_z),0,0],
                                     [-m.sin(fi_z),m.cos(fi_z),0,0],
                                     [0,0,1,0],
                                     [0,0,0,1]])
        else:
            rot_z_matrix = m.eye(4)
        
        rotation_matrix = rot_x_matrix * rot_y_matrix * rot_z_matrix
        #nasobeni matic - operator * pro mpmath
        
        #new posititon of origin
        #tmp_transform_matrix = rotation_matrix * trans_matrix_to_rot_pt
        translated_origin = trans_matrix_to_rot_pt * new_origin
        translated_origin[3] = m.mpf(0) #this will be added to m.eye matrix
        trans_vector_to_transl_og = translated_origin*(-1)
        trans_matrix_to_transl_og = m.eye(4)
        trans_matrix_to_transl_og[:,3] = (trans_matrix_to_transl_og[:,3] +
                                          trans_vector_to_transl_og)
        #new origin must move with rotation of coordinates  =>
        # => OG is not transformed with rotation
        #if OG is transformed with rotation  =>
        # => it is a set point in space, it doesnt move
        
        transform_matrix = trans_matrix_to_transl_og * rotation_matrix * trans_matrix_to_rot_pt
        #transform_matrix = trans_matrix_to_trans_og * rotation_matrix * trans_matrix_to_rot_pt
        #print(transform_matrix)
        if to_starting_pos:
            new_matrix = transform_matrix**-1
            verify_matrix = new_matrix*transform_matrix
            for i in range(4):
                for j in range(4):
                    if i == j:
                        if not m.almosteq(verify_matrix[i,j],m.mpf(1)):
                            warnings.warn("Warning: Inverzni transformace "+
                                          "a puvodni transformace nedali "+
                                          "jednotkovou matici.")
                    else:
                        if not m.almosteq(verify_matrix[i,j],m.mpf(0)):
                            warnings.warn("Warning: Inverzni transformace "+
                                          "a puvodni transformace nedali "+
                                          "jednotkovou matici.")
            transform_matrix = new_matrix  
              
        #poradi rotace @ translace je dulezite, protoze nejdriv se posune 
        #soustava a pak se rotuje
        new_position = np.empty_like(self.position)
        new_direction = np.empty_like(self.direction)
        for i, (point, vector) in enumerate(zip(self.position, self.direction)):
            tmp_point = m.matrix(point.tolist())
            tmp_vector = m.matrix(vector.tolist())
            new_point = transform_matrix*tmp_point
            new_vector = transform_matrix*tmp_vector
            new_position[i] = np.array(new_point.tolist(),
                                       dtype=np.object).reshape(4)
            new_direction[i] = np.array(new_vector.tolist(),
                                        dtype=np.object).reshape(4)
        self.position = new_position
        self.direction = new_direction
        self.normalize_direction() #normalizovat asi neni potreba,
        #ale netransformuji tak casto, takze to nevadi
        F.restore_dps(OG_dps)
    
    
    def propagate_to_boundary_and_refract(self, pos_z, fce_boundary, refr_index_curr,
                                          refr_index_next, elem_radius_outer = None,
                                          elem_radius_inner = None):
        """
        Propagate to boundary a refract with additional try-except-else.
        Used in class method propagate_through_opt_elem.

        Parameters
        ----------
        pos_z : m.mpf, str, int
            position of middle boundary in z-axis in current coordinate system 
        fce_boundary : function
            function of boundary (input - (x,y,bool), output z(x,y) or normal
                                  vector to surface in [x,y,z])
        refr_index_curr : m.mpf, str, int
            refractive index in front of boundary (current refr. index)
        refr_index_next : m.mpf, str, int
            refractive index outside of boundary (next refr. index)
        elem_radius_outer : m.mpf, str, int
            radius of the optical element. Used to put bounds on finding
            intersection between ray and boundary.
        elem_radius_inner : m.mpf, str, int
            radius of hole in optical element. Rays with distance from middle
            smaller than elem_radius_inner are deleted.

        Returns
        -------
        None.
        """
        refr_index_curr = m.mpf(refr_index_curr)
        refr_index_next = m.mpf(refr_index_next)
        pos_z = m.mpf(pos_z)
        if elem_radius_outer:
            elem_radius_outer = m.mpf(elem_radius_outer)
        if elem_radius_inner:
            elem_radius_inner = m.mpf(elem_radius_inner)
        OG_dps = F.upgrade_dps(Config.dps_best)
        new_position = np.empty_like(self.position)
        new_direction = np.empty_like(self.direction)
        for i, (point, vector) in enumerate(zip(self.position, self.direction)):
            if not vector[2]:
                warnings.warn("smer paprsku je kolmy na osu z\n"+
                              f"{i}, {point}, {vector.astype(str)}")
            try:
                t0 = (pos_z-point[2])/vector[2] #(pos_z-z_0)/v_z
            except RuntimeWarning as msg:
                print(msg)
                print("chyba v urceni t0")
                print(f"i={i}, pos={point}, direction={vector.astype(str)}")
            if not elem_radius_outer:
                t_boundary = [(None,None)]
            elif not vector[0] and not vector[1]: #parallel ray with z-axis
                if np.linalg.norm(point[0:2]) > elem_radius_outer:
                    print(f"i={i}, pos={point}, direction={vector.astype(str)}")
                    warnings.warn("Parallel beam doesnt intersect optical element.")
                else:
                    t_boundary = [(None, None)]
            else:
                #find boundaries of parameter t, so x**2+y**2<elem_radius_outer**2
                #v deniku 14.5.2021
                vect2 = vector[0]**2+vector[1]**2
                diskrim = (-(vector[1]*point[0]-vector[0]*point[1])**2+
                           elem_radius_outer**2*vect2)
                t_boundary = [((-vector[0]*point[0]-vector[1]*point[1]-
                                m.sqrt(diskrim))/vect2,
                                (-vector[0]*point[0]-vector[1]*point[1]+
                                m.sqrt(diskrim))/vect2)]
                t_boundary = np.sort(t_boundary)
                if t0 < t_boundary[0][0] or t0 > t_boundary[0][1]:
                        t0 = (t_boundary[0]+t_boundary[1])/2
            correct = False
            for attempt in range(5): #kdyby pocatecni odhad nebyl vhodny
                try:
                    cons = ({'type': 'ineq',
                             'fun': F.cons1_ray_on_boundary_to_solve,
                             'args': (np.array(point,dtype=np.float64),
                                      np.array(vector,dtype=np.float64),
                                      fce_boundary, float(pos_z))},
                            {'type': 'ineq',
                             'fun': F.cons2_ray_on_boundary_to_solve,
                             'args': (np.array(point,dtype=np.float64),
                                      np.array(vector,dtype=np.float64),
                                      fce_boundary, float(pos_z))})
                    t_res = scipy.optimize.minimize(lambda t: t, t0, tol=1e-4,
                                                     bounds = t_boundary,
                                                     constraints = cons)
                    key_args = {"ray_start_pos":point,"ray_vector":vector,
                                "fce_boundary":fce_boundary, "pos_in_z":pos_z}
                    t1,t2 = (m.mpf(t_res.x[0]*0.99),m.mpf(t_res.x[0]*1.01))
                    t_root = m.findroot(lambda x: F.ray_on_boundary_to_solve(x, 
                                                         **key_args),
                                        [t1,t2],
                                        solver='illinois',
                                        tol = Config.tol_find_t)
                    tmp_new_point = (point+t_root*vector)
                except RuntimeWarning as msg:
                    print(f"attempt number {attempt} in t_root finding")
                    print(msg)
                    continue
                else:
                    if(m.almosteq(tmp_new_point[2],fce_boundary(tmp_new_point[0],
                                                                 tmp_new_point[1],
                                                                 False)+pos_z,
                                   rel_eps=0, abs_eps=Config.tol_find_t)): 
                        #pocitam v mm =>presnost 1e-8 je presnost v 10 pm
                        new_position[i] = tmp_new_point
                        correct = True
                        break
                    else:
                        if t_boundary[0][0] and t_boundary[0][1]:
                            t0 = np.random.uniform(low=t_boundary[0][0],
                                                   high=t_boundary[0][1])
                        else:
                            t0 = t_root
                        continue
            if not correct:
                warnings.warn("Point on boundary not found/ not precise \n"+
                              f" {i}, {point}, {vector} \nvypisuji z hodnoty"+
                              f" {tmp_new_point[2]}, {fce_boundary(tmp_new_point[0],tmp_new_point[1],False)}")
            if elem_radius_inner:
                if np.linalg.norm(point[0:2]) < elem_radius_inner:
                    new_position[i] = [None]*4 
                    new_direction[i] = [None]*4
                    continue #continue to next ray (iteration of for cycle)
            normala = fce_boundary(new_position[i][0],
                                   new_position[i][1], normala = True)*(-1)
            normala = normala/np.linalg.norm(normala)
            if F.vectors_angle(vector[:-1], normala) >= m.pi/2:
                normala = normala*(-1)
                print("Turning normal vector")
            correct = False
            try:
                #lom pocitan podle
                #https://physics.stackexchange.com/questions/435512/snells-law-in-vector-form
				#stejne jako clanek
				#Optical Ray Tracing Using Parallel Processors
                vector_root = m.matrix([0,0,0,0])
                m_vector = m.matrix(vector)
                m_normala = m.matrix(normala)
                mu = refr_index_curr/refr_index_next
                norm_dot_in = (m_normala.T*m_vector[:-1])[0]
                tmp1 = m.sqrt(1-mu**2*(1-norm_dot_in**2))*m_normala
                tmp2 = mu*(m_vector[:-1]-norm_dot_in*m_normala)
                vector_root[:-1] = (tmp1+tmp2)/m.norm(tmp1+tmp2)
            except RuntimeWarning as msg:
                print("RuntimeWarning in vector_root finding")
                print(msg)
                #print("chyba v nalezeni refraktovaneho vektoru")
                #print(f"{i}, {point}, {vector}")
            else:
                #check if cross vectors are linearly dependent (angle = 0)
                #and the ratio is correct
                diff_k = (refr_index_next*vector_root-
                          refr_index_curr*m_vector)[0:-1]
                n1,n2,n3 = m_normala
                d1,d2,d3 = diff_k
                c1 = d2*n3-n2*d3
                c2 = d3*n1-n3*d1
                c3 = d1*n2-n1*d2
                if(m.almosteq(c1,m.mpf(0),
                               rel_eps=0, abs_eps=Config.tol_refract) and
                   m.almosteq(c2,m.mpf(0),
                               rel_eps=0, abs_eps=Config.tol_refract) and
                   m.almosteq(c3,m.mpf(0),
                               rel_eps=0, abs_eps=Config.tol_refract)):
                    if len(vector_root) != 4:
                        warnings.warn("Vector root's length is not 4")
                    new_direction[i] = np.array([x for x in vector_root],
                                                dtype=np.object)
                    correct = True
            if not correct:
                uhel = (F.vectors_angle(np.cross(refr_index_next*
                                                 np.array([x for x in vector_root],
                                                          dtype=np.object)[:-1], 
                                                 normala),
                                        np.cross(refr_index_curr*
                                                 np.array([x for x in vector_root],
                                                          dtype=np.object), 
                                                 normala)))
                warnings.warn("Refracted vector not accurate enough\n"+
                              f"uhel mezi vektory {uhel.astype(str)}\n"
                              f"i={i},\nposition={point},\ndirection={vector}\n"+
                              f"diff_k x normala = {np.cross(diff_k,normala).astype(str)}\n"+
                              f"{(refr_index_next*np.linalg.norm(vector[0:-1])).astype(str)}\n"+
                              f"{(refr_index_curr*np.linalg.norm(vector_root[0:-1])).astype(str)}")
        #remove None values
        ind_to_del = []
        for i,vect in enumerate(new_position):
            if vect[0] is None:
                ind_to_del.append(i)
        new_position = np.delete(new_position, ind_to_del, axis = 0)
        new_direction = np.delete(new_direction, ind_to_del, axis = 0)
        self.position = new_position
        self.direction = new_direction
        self.normalize_direction()
        F.restore_dps(OG_dps)
        
        
    def propagate_through_opt_elem(self, elem):
        """
        Funkce k propagaci paprsku skrz opticky element.
        Hleda se podle toho, kdy se rovna [x,y,z] paprsku a nejdriv jednoho 
        a pak druheho rozhrani.
        
        Parameters
        ----------
        elem : class Opt_element
            opticky element

        Returns
        -------
        None.
        """
        
        OG_dps = F.upgrade_dps(Config.dps_best)
        #transform to coordinate system of optical element
        self.transform(elem.origin, elem.rotation_angles,
                       elem.pt_of_rot, False)        
        #propagate to first boundary and refract
        self.propagate_to_boundary_and_refract(0,
                                               elem.fce_boundary_in,
                                               self.refr_index,
                                               elem.refr_index,
                                               elem.radius_outer,
                                               elem.radius_inner)
        print("Propagated through first boundary.")
        #propagate to second boundary and refract
        self.propagate_to_boundary_and_refract(elem.thickness,
                                               elem.fce_boundary_out,
                                               elem.refr_index,
                                               self.refr_index,
                                               elem.radius_outer,
                                               elem.radius_inner)
        print("Propagated through second boundary")
        #transform to original coordinate system of the rays
        self.transform(elem.origin, elem.rotation_angles,
                       elem.pt_of_rot, True)
        F.restore_dps(OG_dps)
        
    def radius(self, z, way):
        """
        Counts radius in position z using specific way of counting.

        Parameters
        ----------
        way : str
            specific way of counting radius ("RMS", "GEO")
            
        z : str, int, m.mpf
            position in which the radius is counted

        Returns
        -------
        m.mpf
            returns radius

        """
        OG_dps = F.upgrade_dps(Config.dps_best)
        try:
            z = m.mpf(z)
        except TypeError:
            z = m.mpf(z[0])
        if self.position.shape[0] == 1:
            z_diff = z - self.position[0][2]
            t = z_diff/self.direction[0][2]
            tmp_position = self.position[0] + self.direction[0]*t
            return np.linalg.norm(tmp_position[0:2])
        tmp_position = np.empty_like(self.position)
        for i, (point, vector) in enumerate(zip(self.position, self.direction)):
            z_diff = z - point[2]
            t = z_diff/vector[2]
            tmp_position[i] = point + vector*t
        centre_of_mass_xy = np.mean(tmp_position[:,0:2], axis = 0) #works
        tmp_position[:,0:2] = tmp_position[:,0:2] - centre_of_mass_xy
        if way == "RMS":
            r2sum = (tmp_position[:,0]**2+tmp_position[:,1]**2).sum()
            rRMS = m.sqrt(r2sum/tmp_position.shape[0])
            F.restore_dps(OG_dps)
            return rRMS
        elif way == "GEO":
            radii2 = tmp_position[:,0]**2+tmp_position[:,1]**2
            rGEO = m.sqrt(np.max(radii2))
            F.restore_dps(OG_dps)
            return rGEO
            
    def radius_scipy(self, z, way):
        """
        Used to aproximately find minimal radius using scipy.optimize.minimize.
        Counts radius in position z using specific way of counting.

        Parameters
        ----------
        way : str
            specific way of counting radius ("RMS", "GEO")
            
        z : float
            position in which the radius is counted

        Returns
        -------
        float
            returns radius

        """
        float_position = np.array(self.position,dtype=np.float64)
        float_direction = np.array(self.direction,dtype=np.float64)
        if float_position.shape[0] == 1:
            z_diff = z - float_position[0][2]
            t = z_diff/float_direction[0][2]
            tmp_position = float_position[0] + float_direction[0]*t
            return np.linalg.norm(tmp_position[0:2])
        tmp_position = np.empty_like(float_position)
        for i, (point, vector) in enumerate(zip(float_position, float_direction)):
            z_diff = z - point[2]
            t = z_diff/vector[2]
            tmp_position[i] = point + vector*t
        centre_of_mass_xy = np.mean(tmp_position[:,0:2], axis = 0) #works
        tmp_position[:,0:2] = tmp_position[:,0:2] - centre_of_mass_xy
        if way == "RMS":
            r2sum = (tmp_position[:,0]**2+tmp_position[:,1]**2).sum()
            return np.sqrt(r2sum/tmp_position.shape[0])
        elif way == "GEO":
            radii2 = tmp_position[:,0]**2+tmp_position[:,1]**2
            return np.sqrt(np.max(radii2))
        
        
    def find_smallest_radius(self, way):
        """
        Finds smallest radius defined by specific way.

        Parameters
        ----------
        way : str
            specific way of counting radius ("RMS", "GEO")

        Returns
        -------
        m.mpf
            z position of the smallest radius

        """
        #chosing a ray not parallel with z-axis
        done = False
        ray = int(self.position.shape[0]/2)
        while not done:
            if self.direction[ray,0]:
                t = -self.position[ray,0]/self.direction[ray,0]
                done = True
            elif self.direction[ray,1]:
                t = -self.position[ray,1]/self.direction[ray,1]
                done = True
            else:
                ray += 1
        x0,y0,z0,_ = self.position[ray]+self.direction[ray]*t
        #check if fokus is a single point
        if m.almosteq(x0,0,abs_eps=1e-15) and m.almosteq(y0,0,abs_eps=1e-15):
            fokus_size = self.radius(z0, way)
            if m.almosteq(fokus_size, 0, abs_eps=1e-15, rel_eps=0):
                return z0
        result = scipy.optimize.minimize(self.radius_scipy, float(z0), args=(way),
                                         tol=1e-2)
        #find minimal radius by finding root of derivative
        z1,z2 = m.mpf(result.x[0]*0.99), m.mpf(result.x[0]*1.01)
        OG_dps = F.upgrade_dps(Config.dps_best)
        m_res = m.findroot(lambda z: m.diff(lambda x: self.radius(x, way),z),
                           x0 = [z1,z2],solver='illinois')
        F.restore_dps(OG_dps)
        return m_res
        
        
class Opt_element:
    def __init__(self, origin, rotation_angles, pt_of_rot, thickness, refr_index,
                 fce_boundary_in, fce_boundary_out, radius_outer = None,
                 radius_inner = None):
        """
        Inicializator tridy Opt_element (opticky prvek).
        
        Parameters
        ----------
        origin : [x,y,z] elements are m.mpf, str, int
            poloha zacatku opt. prvku  v soustave, ve ktere se 
            zakladne popisuji paprsky (funkce generujici rozhrani z(x,y)
            pak urcuje, kam rozhrani miri)
        rotation_angles : [fi_x, fi_y, fi_z] elements are m.mpf, str, int
            uhly v radianech, o ktere je
            opt. prvek rotovan vuci soustave, kde se zakladne popisuji paprsky.
            x,y,z odpovida ose rotace (kolem ktere osy rotuji).
            Znamenko uhlu podle pravidla prave ruky (palec ve smeru osy,
            kolem ktere rotuji), tj. prsty od puvodni ose k nove => kladny uhel.
        pt_of_rot : [x,y,z] elements are m.mpf, str, int
            bod, kolem ktereho se rotuje
        thickness: m.mpf, str, int
            vzdalenost stredu rozhrani
        refr_index : m.mpf, str, int
            popisuje index lomu optickeho prvku
        fce_boundary_in : fce s parametry(x,y,bool),
            popisuje vstupni rozhrani optickeho prvku podle zavilosti z(x,y).
            bool = True => vracim normalovy vektro k rozhrani v body [x,y,z(x,y)]
            bool = False => vracim z(x,y)
            pripadne limitujici rozmery jsou v teto funkci.
        fce_boundary_out : fce s parametry(x,y,bool),
            popisuje vystupni rozhrani optickeho prvku podle zavilosti z(x,y).
            bool = True => vracim normalovy vektro k rozhrani v body [x,y,z(x,y)]
            bool = False => vracim z(x,y)
            pripadne limitujici rozmery jsou v teto funkci.
        radius_outer : m.mpf, str, int
            radius of the optical element. It sets the boundary in finding 
            intersection between ray and boundary function. None means there
            is no limitation.
        radius_inner : m.mpf, str, int
            radius of hole in optical element. Rays with distance from middle
            smaller than elem_radius_inner are deleted.
        Returns
        -------
        None.
        """
        self.origin = np.array([m.mpf(x) for x in origin],dtype=np.object)
        self.rotation_angles = np.array([m.mpf(x) for x in rotation_angles],
                                         dtype=np.object)
        if self.origin.shape != (3,) or self.rotation_angles.shape != (3,):
            warnings.warn("Warning: Pocatek a uhel rotace nejsou vektory 3x1")
        self.pt_of_rot = np.array([m.mpf(x) for x in pt_of_rot],dtype=np.object)
        self.thickness = m.mpf(thickness)
        self.refr_index = m.mpf(refr_index)
        self.fce_boundary_in = fce_boundary_in
        self.fce_boundary_out = fce_boundary_out
        if radius_outer:
            self.radius_outer = m.mpf(radius_outer)
        else:
            self.radius_outer = radius_outer
        if radius_inner:
            self.radius_inner = m.mpf(radius_inner)
        else:
            self.radius_inner = radius_inner

        