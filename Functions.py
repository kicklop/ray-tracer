# -*- coding: utf-8 -*-
"""
Functions file for Ray-Tracing algorithm. All values of length in mm 
(because function of PALS lens is in mm). Written in mpmath

Created on Wed May 19 20:56:04 2021

@author: Acer
"""

import numpy as np
import mpmath as m
import warnings
import Config


def gen_equid_points_on_disk(num_pts, radius, method):
    """
    Generuj ekvidistantni body uvnitr kruhu.
    
    Parameters
    ----------
    num_pts : int,
        udava pocet generovanych bodu
    
    radius : mpf
        udava polomer kruhu

    method : string, ("sunflower", "rays", "line_x","line_y")
        urci, jakou metodou se maji rozdelit body v kruhu

    Returns
    -------
    x,y : tuple dvou numpy array, ktere odpovidaji souradnicim bodu x,y 
    """
    if not isinstance(num_pts,int):
        warnings.warn("num_pts isnt integer in gen_equid_points_on_disk")
    #slunecnice:
    #https://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere
    if method == "sunflower":
        indices = np.arange(m.mpf("0"), m.mpf(num_pts)) + m.mpf('0.5')
        r = np.empty_like(indices)
        x = np.empty_like(indices)
        y = np.empty_like(indices)
        theta = m.pi * (1 + 5**m.mpf("0.5")) * indices
        for i, index in enumerate(indices):
            r[i] = m.sqrt(index/num_pts)*m.mpf(radius)
            x[i] = r[i]*m.cos(theta[i])
            y[i] = r[i]*m.sin(theta[i])
        return x,y
    #cary do stredu, na kterych jsou body
    elif method == "rays":
        num_of_lines = m.mpf(10)
        x = np.array([m.mpf(0)])
        y = np.array([m.mpf(0)])
        for k in m.arange(num_of_lines):
            theta = 2*m.pi/num_of_lines*(k+1)
            for j in m.arange(int(num_pts/num_of_lines)):
                r = m.mpf(radius)*(j+1)/(int(num_pts/num_of_lines))
                x = np.append(x,r*m.cos(theta))
                y = np.append(y,r*m.sin(theta))
        while len(x) > num_pts:
            x = np.delete(x,-1)
            y = np.delete(y,-1)
        while len(x) < num_pts:
            x = np.append(x,m.mpf(0))
            y = np.append(y,m.mpf(0))
        return x,y
    #cela hrana je pokryta body    
    #http://www.holoborodko.com/pavel/2015/07/23/generating-equidistant-points-on-unit-disk/
    elif method == "line_x":
        x = m.linspace(m.mpf(-radius), m.mpf(radius), num_pts)
        x = np.array(x)
        y = m.linspace(0, 0, num_pts)
        y = np.array(y)
        return x,y
    elif method == "line_y":
        y = m.linspace(m.mpf(-radius), m.mpf(radius), num_pts)
        y = np.array(y)
        x = m.linspace(0, 0, num_pts)
        x = np.array(x)
        return x,y

def PALS_lens_polynom(x,y,normala):
    """
    Zakrivena cast PALSovske cocky.
    
    Parameters
    ---------
    x,y : m.mpf numbers
        udavaji polohy bodu x,y, kde se vycisli funkce
        
    normala : bool
        False znamena, ze chci vratit bod plochy z(x,y)
        True znamena, ze chci vratit normalovy vektor na plochu v bode [x,y,z(x,y)]
    Returns
    -------
    numpy array
    vrati vycislene hodnoty polynomu v bodech x,y, nebo normalu na plochu 
    [df/dx,df/dy,-1] v bode [x,y,f(x,y)]
    """
    a = m.mpf("0.003165739")
    b = m.mpf("0.01635294")
    c = m.mpf("0.22274610e-8")
    d = m.mpf("0.15369828e-13")
    e = m.mpf("0.17898972e-18")
    if not normala:
        r2 = m.mpf(x)**2+m.mpf(y)**2
        result = (a*r2/(1+m.sqrt(1-(1-b)*a**2*r2))- c*r2**2 -d*r2**3 - e*r2**4)
# =============================================================================
#         result = (0.003165739*(x**2+y**2)/(1+np.sqrt(1-(1-0.01635294)*
#              0.003165739**2*(x**2+y**2)))-0.22274610e-8*(x**2+y**2)**2-
#              0.15369828e-13*(x**2+y**2)**3-0.17898972e-18*(x**2+y**2)**4)
# =============================================================================
    elif normala:
        result = np.array([m.mpf(0),m.mpf(0),m.mpf(-1)])
        result[0] = (a**3*x*(1 - b)*(x**2 + y**2)/
                    (m.sqrt(-a**2*(1 - b)*(x**2 + y**2) + 1)*
                    (m.sqrt(-a**2*(1 - b)*(x**2 + y**2) + 1) + 1)**2) + 
                    2*a*x/(np.sqrt(-a**2*(1 - b)*
                    (x**2 + y**2) + 1) + 1) - 4*c*x*(x**2 + y**2) -
                    6*d*x*(x**2 + y**2)**2 - 8*e*x*(x**2 + y**2)**3)
        result[1] = (a**3*y*(1 - b)*(x**2 + y**2)/
                    (m.sqrt(-a**2*(1 - b)*(x**2 + y**2) + 1)*
                    (m.sqrt(-a**2*(1 - b)*(x**2 + y**2) + 1) + 1)**2) + 
                    2*a*y/(m.sqrt(-a**2*(1 - b)*(x**2 + y**2) + 1) + 1) -
                    4*c*y*(x**2 + y**2) - 6*d*y*(x**2 + y**2)**2 -
                    8*e*y*(x**2 + y**2)**3)
    return result


def primka(x,y, normala):
    """
    Rovina pro lom.
    
    Parameters
    ---------
    x,y : m.mpf numbers
        udavaji polohy bodu x,y, kde se vycisli funkce
        
    normala : bool
        False znamena, ze chci vratit bod plochy z(x,y)
        True znamena, ze chci vratit normalovy vektor na plochu v bode [x,y,z(x,y)]
    Returns
    -------
    numpy array
    vrati vycislene hodnoty polynomu v bodech x,y, nebo normalu na plochu 
    [df/dx,df/dy,-1] v bode [x,y,f(x,y)]
    """
    if not normala:
        result = m.mpf(0)
    elif normala:
        result = np.array([m.mpf(0),m.mpf(0),m.mpf(-1)])
    return result


def cons1_ray_on_boundary_to_solve(t, ray_start_pos, ray_vector, fce_boundary,
                                   pos_in_z):
    """
    Constraint, that is used in scipy.optmize.minimize to meet
    the ray and boundary.
    
    Together with cons2_ray_on_boundary_to_solve, they minimize absolute
    value of pos[2]-fce_boundary(pos[0],pos[1], normala=False) - pos_in_z

    Parameters
    ----------
    t : parameter for minimize
    ray_start_pos : [x,y,z,1]
        starting position of ray
    ray_vector : [v_x,v_y,v_z,0]
        direction of ray
    fce_boundary : function
        function of boundary. input is x,y,bool. Output is z or normal vector
        depending no bool (True => normal vector, False => z).
    pos_in_z : float
        position in z-axis of middle of boundary.
        (Mainly thickness of optical element.)

    Returns
    -------
    solver should find minimum. It should be equal to finding root.
    """
    pos = ray_start_pos + t*ray_vector
    return pos[2]-float(fce_boundary(pos[0],pos[1], normala=False)) - pos_in_z

def cons2_ray_on_boundary_to_solve(t, ray_start_pos, ray_vector, fce_boundary,
                                   pos_in_z):
    """
    Second constraint. Parameters same as cons1_ray_on_boundary_to_solve.
    """
    pos = ray_start_pos + t*ray_vector
    return -(pos[2]-float(fce_boundary(pos[0],pos[1], normala=False)) - pos_in_z)

def ray_on_boundary_to_solve(t, ray_start_pos, ray_vector, fce_boundary,
                                   pos_in_z):
    """
    Find the root of pos[2]-fce_boundary(pos[0],pos[1], normala=False) - pos_in_z
    using mpmath solver Illinois.

    Parameters
    ----------
    t : parameter for which root is found, m.mpf
    ray_start_pos : [x,y,z,1] vector of mp.mpf
        starting position of ray
    ray_vector : [v_x,v_y,v_z,0] vector of m.mpf
        direction of ray
    fce_boundary : function
        function of boundary. input is x,y,bool. Output is z or normal vector
        depending no bool (True => normal vector, False => z).
    pos_in_z : mpmpf
        position in z-axis of middle of boundary.
        (Mainly thickness of optical element.)

    Returns
    -------
    solver should find minimum. It should be equal to finding root.
    """
    pos = ray_start_pos + t*ray_vector
    return pos[2]-fce_boundary(pos[0],pos[1], normala=False) - pos_in_z


def vectors_angle(v1, v2):
    """
    Measure angle between two vectors.

    Parameters
    ----------
    v1 : array of m.mpf numbers
        prvni vekor
    v2 : array of m.mpf numbers
        druhy vektor

    Returns
    -------
    Angle between vectors in radians.
    """
    v1_zeros = True #bool stating if v1 is full of zeros
    v2_zeros = True
    for el1, el2 in zip(v1,v2):
        #if zero was found then we dont need to check equality to 0
        if v1_zeros and not m.almosteq(el1, 0):
            v1_zeros = False
        if v2_zeros and not m.almosteq(el2, 0):
            v2_zeros = False
    if v1_zeros: #if v1 is vector of zeros
        if v2_zeros: #if v2 is vector of zeros
            return 0
        else:
            warnings.warn("Measuring angle between zero and non-zero vector.")
    elif v2_zeros: #v1 is not vector of zeros and v2 is vector of zeros
        warnings.warn("Measuring angle between zero and non-zero vector.")
    #numpy norm works same as m.norm for mpmath vector
    unit_v1 = v1 / np.linalg.norm(v1)
    unit_v2 = v2 / np.linalg.norm(v2)
    return m.acos(np.dot(unit_v1, unit_v2))


def upgrade_dps(new_pot_dps):
    """
    Increases decimal point precision (dps) to new_pot_dps,
    if new_pot_dps > m.mp.dps, otherwise it stays same.
    Returns the original dps.

    Parameters
    ----------
    new_pot_dps : int
        New potential dps.

    Returns
    -------
    OG_dps : int
        dps before calling the function.

    """
    OG_dps = m.mp.dps
    if new_pot_dps > m.mp.dps:
        m.mp.dps = new_pot_dps
    return OG_dps

def restore_dps(OG_dps):
    """
    Restore dps after calling upgrade_dps.

    Parameters
    ----------
    OG_dps : int
        returned value from function upgrade_dps

    Returns
    -------
    None.

    """
    m.mp.dps = OG_dps
    







